# Building NGINX Plus AMI

You can pre-build a NGINX+ image with Packer and then reference the resulting AMI in your Terraform build.

## Prerequisites

- [Packer](https://www.packer.io/downloads)
- [NGINX Plus certificate and public key](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-plus/#prerequisites)

## Instructions

1. Create `variables.pkrvars.hcl` in this directory with the following variables
      | Variable | Description |
      | --- | --- |
      | `version` | Suffic to append to AMI names |
      | `region` | AWS region in which Packer builds and publishes AMIs |
      | `nginx_repo_cert_path` | Full path to certificate for accessing NGINX repo |
      | `nginx_repo_key_path` | Full path to key for accessing NGINX repo |
      | `prefix` | Prefix for the AMI name generated |
      See [variables.pkrvars.hcl.example](./variables.pkrvars.hcl.example) for an example.
1. Build the images with the following command

```bash
      packer build -var-file=variables.pkrvars.hcl nginx
```

The above command will output the NGINX+ AMI ID, as below, which will be used as inputs to the Terraform project for deploying the EC2

```bash
==> Builds finished. The artifacts of successful builds are:
--> nginx-1.0.0.amazon-ebs.ubuntu: AMIs were created:
ap-southeast-2: ami-04f231eafafd660d3
```
