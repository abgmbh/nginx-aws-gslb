./scripts/buildNPlusWithAgent.sh -t npluswithagent -n https://acm.acm-demo.aws.abbagmbh.de

sudo iptables -A FORWARD -j ACCEPT 

version: '3'
services:
     nginx1:
         image: npluswithagent:latest
         networks:
           - nginx_net_1
         hostname: nginx1
         environment:
           - NIM_HOST=acm.acm-demo.aws.abbagmbh.de
           - NIM_GRPC_PORT=443
           - NIM_INSTANCEGROUP=lb1

networks:
  nginx_net_1:
    ipam:
      driver: default
      config:
        - subnet: "192.168.1.0/24"


version: '3'
services:
     nginx1:
         image: npluswithagent:latest
         networks:
           - nginx_net_2
         hostname: nginx2
         environment:
           - NIM_HOST=acm.acm-demo.aws.abbagmbh.de
           - NIM_GRPC_PORT=443
           - NIM_INSTANCEGROUP=lb1

networks:
  nginx_net_1:
    ipam:
      driver: default
      config:
        - subnet: "192.168.1.0/24"


docker network create --driver bridge --subnet 192.168.1.0/24 --ip-range 192.168.1.0/24 nginx

docker run -d --name n1 --net nginx nginx

docker network create --driver bridge --subnet 192.168.2.0/24 --ip-range 192.168.2.0/24 nginx

docker run -d --name n2 --net nginx nginx