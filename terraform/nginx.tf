resource "aws_security_group" "nginx" {
  name   = "${random_id.id.dec}-nginx"
  vpc_id = module.vpc.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${local.my_ip}/32"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${local.my_ip}/32", local.vpc_cidr, "0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${local.my_ip}/32", local.vpc_cidr]
  }

  ingress {
    description = "ping"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["${local.my_ip}/32", local.vpc_cidr, "0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_launch_configuration" "asg_launchConfig_nginxPlus" {
  name_prefix     = "${random_id.id.dec}-nginx-"
  image_id        = var.nginxPlus_ami
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.ssh_access.key_name
  security_groups = [aws_security_group.nginx.id]
  user_data = templatefile("init.sh.tpl",
    {
      user  = var.user
      pass  = var.pass
      group = var.group
      ltm   = var.ltm
      gtm   = var.gtm
    }
  )
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg_nginxPlus" {

  launch_configuration = aws_launch_configuration.asg_launchConfig_nginxPlus.name
  vpc_zone_identifier  = module.vpc.public_subnets
  min_size             = 0
  max_size             = 4
  desired_capacity     = var.nginxPlus_count

  tag {
    key                 = "Name"
    value               = "nginx-g1"
    propagate_at_launch = true
  }
}


