resource "aws_instance" "docker_hosts" {
  count                  = 2
  ami                    = var.ubuntu_ami
  instance_type          = "t2.medium"
  key_name               = aws_key_pair.ssh_access.key_name
  subnet_id              = module.vpc.public_subnets[count.index]
  vpc_security_group_ids = [aws_security_group.nginx.id]

  source_dest_check = false

  tags = {
    Name = "cz_docker_${count.index + 1}"
  }
}
