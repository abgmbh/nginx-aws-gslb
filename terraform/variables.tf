variable "region" {
  description = "Region to deploy AWS resources in"
  type        = string
  default     = ""
}

variable "nginxPlus_ami" {
  description = "AMI ID for NGINX Plus instances"
  type        = string
  default     = ""
}

variable "ubuntu_ami" {
  description = "AMI ID for ubuntu instances"
  type        = string
  default     = ""
}

variable "nginxPlus_count" {
  description = "Number of NGINX Plus instances to be deployed"
  type        = number
  default     = 0
}

variable "ssh_public_key" {
  description = "SSH public key to be loaded onto all EC2 instances for SSH access"
  type        = string
  default     = ""
}

variable "resource_tags" {
  type    = map(string)
  default = {}
}

variable "ltm" {
  description = "ltm address"
  type        = string
  default     = ""
}

variable "gtm" {
  description = "gtm address"
  type        = string
  default     = ""
}

variable "user" {
  description = "BIG-IP user"
  type        = string
  default     = ""
}

variable "pass" {
  description = "BIG-IP pass"
  type        = string
  default     = ""
}

variable "group" {
  description = "NGINX instance group ID"
  type        = string
  default     = ""
}

variable "ecs_cluster_name" {
  description = "ECS cluster name"
  type        = string
  default     = ""
}

variable "ecs_instance_count" {
  description = "ECS instance count"
  type        = number
  default     = 2
}

variable "ecs_instance_type" {
  description = "ECS instance type"
  type        = string
  default     = ""
}

