#!/usr/bin/env python3.11

import boto3
import requests
import base64
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def send_curl_request(url, method='GET', data=None):
    user = 'bigipuser'
    password = 'YATHzLV1pFOAJsCu'
    CRED_B64 = base64.b64encode(
        (user + ':' + password).encode('utf-8')).decode('utf-8')

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + CRED_B64
    }
    request_data = None if data == None else json.dumps(data)
    response = requests.request(
        method, url, data=request_data, headers=headers, verify=False)

    if response.status_code == 200:
        if method == 'GET':
            return response.json()
        else:
            print(response.text)
            return response.text
    else:
        raise Exception(f'Request failed. Status code: {response.status_code}')


# Create an EC2 client
ec2 = boto3.client('ec2')

# Get instances with a Name tag value of "nginx"
response = ec2.describe_instances(
    Filters=[
        {'Name': 'tag:Name',
         'Values': ['nginx-g1']}
    ]
)

# Extract the private IP addresses and public DNS names of the instances

private_ip_addresses = []

for reservation in response['Reservations']:
    for instance in reservation['Instances']:
        if instance['State']['Name'] != 'terminated':
            private_ip_addresses.append(instance['PrivateIpAddress'])

print(private_ip_addresses)

public_dns_names = []

for reservation in response['Reservations']:
    for instance in reservation['Instances']:
        public_dns_names.append(instance['PublicDnsName'])
print(public_dns_names)


gtm = '13.210.37.219'

url = f"https://{gtm}/mgmt/tm/sys/config"
wideip_a_url = f'https://{gtm}/mgmt/tm/gtm/wideip/a'
pool_url = f'https://{gtm}/mgmt/tm/gtm/pool/a'

payload = {
    "command": "save"
}

wideip_a_content = send_curl_request(wideip_a_url, method='GET')
wideip_a_names = [item['name'] for item in wideip_a_content['items']]
print(wideip_a_names)

pool_content = send_curl_request(pool_url, method='GET')
pool_names = [item['name'] for item in pool_content['items']]
print(pool_names)

# Delete stale wideIP
for name in set(wideip_a_names) - set(public_dns_names):
    print(name)
    wideip_url = f"https://{gtm}/mgmt/tm/gtm/wideip/a/~Common~{name}"
    print(wideip_url)
    send_curl_request(wideip_url, method='DELETE')
    print(f"Successfully deleted WideIP {name}")

# Delete stale pools
new_octet = 'p_169'
mod_ip = [
    f'{new_octet}.{ip.split(".")[1]}.{ip.split(".")[2]}.{ip.split(".")[3]}' for ip in private_ip_addresses]

for name in set(pool_names) - set(mod_ip):
    print(name)
    pool_url = f"https://{gtm}/mgmt/tm/gtm/pool/a/~Common~{name}"
    print(pool_url)
    send_curl_request(pool_url, method='DELETE')
    print(f"Successfully deleted pool {name}")

# Save config
response = send_curl_request(url, method='POST', data=payload)
if "/var/run/f5pcimap: OK\\n" in response:
    print('Saved')
else:
    print('Save failed')
