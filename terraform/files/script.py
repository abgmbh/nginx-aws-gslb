import requests
import json
import base64
import subprocess
import time
import sys

PRIVATE_IP = requests.get(
    'http://169.254.169.254/latest/meta-data/local-ipv4').text
FQDN = requests.get(
    'http://169.254.169.254/latest/meta-data/public-hostname').text

LTM_VIP = '169' + PRIVATE_IP[3:]

user = 'your-username'
password = 'your-password'
CRED_B64 = base64.b64encode(
    (user + ':' + password).encode('utf-8')).decode('utf-8')

ltm = 'your-ltm'
gtm = 'your-gtm'

node_payload = {
    "name": PRIVATE_IP,
    "address": PRIVATE_IP
}

pool_payload = {
    "name": PRIVATE_IP,
    "monitor": "/Common/http",
    "members": PRIVATE_IP + ":80"
}

virtual_payload = {
    "name": LTM_VIP,
    "destination": LTM_VIP + ":80",
    "ipProtocol": "tcp",
    "profiles": "/Common/fastL4",
    "pool": PRIVATE_IP
}

pool_name = 'p_' + LTM_VIP

gtm_pool_payload = {
    "name": pool_name
}

gtm_member_payload = {
    "name": "/Common/DC1_LTM:/Common/" + LTM_VIP,
    "partition": "Common"
}

gtm_wideip_payload = {
    "name": FQDN,
    "pools": [
        {
            "name": pool_name,
            "partition": "Common",
            "nameReference": {
                "link": "https://localhost/mgmt/tm/gtm/pool/a/~Common~" + pool_name
            }
        }
    ]
}

headers = {
    "Content-Type": "application/json",
    "Authorization": "Basic " + CRED_B64
}


def execute_command(command):
    return subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT).decode('utf-8').strip()


def send_curl_request(url, payload):
    response = requests.post(url, data=json.dumps(
        payload), headers=headers, verify=False)
    return response.status_code


# create a node
node_url = f'https://{ltm}/mgmt/tm/ltm/node'
send_curl_request(node_url, node_payload)

# create a pool
pool_url = f'https://{ltm}/mgmt/tm/ltm/pool'
send_curl_request(pool_url, pool_payload)

# create a virtual
virtual_url = f'https://{ltm}/mgmt/tm/ltm/virtual'
send_curl_request(virtual_url, virtual_payload)

# install nginx-agent
execute_command(
    'curl -k https://acm.acm-demo.aws.abbagmbh.de/install/nginx-agent | bash -s -- --instance-group ${group}')

# start nginx-agent
execute_command('systemctl start nginx-agent')

# create a pool of A type
gtm_pool_url = f'https://{gtm}/mgmt/tm/gtm/pool/a'
send_curl_request(gtm_pool_url, gtm_pool_payload)

# associate member to pool
runtime = time.time() + 300  # 5 minutes

while time.time() <= runtime:
    status = send_curl_request(
        f'https://{gtm}/mgmt/tm/gtm/pool/a/~Common~{pool_name}/members/', gtm_member_payload)
    if status == 200:
        print("Code returned a status 200")
        break
    time.sleep(5)

    if status != 200:
        sys.exit(1)

runtime = time.time() + 300  # 5 minutes

while time.time() <= runtime:
    status = send_curl_request(
        f'https://{gtm}/mgmt/tm/gtm/wideip/a', gtm_member_payload)
    if status == 200:
        print("Code returned a status 200")
        break
    time.sleep(5)

    if status != 200:
        sys.exit(1)
