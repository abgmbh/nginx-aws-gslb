resource "aws_ecs_task_definition" "nginx" {
  family = "nginx"
  container_definitions = jsonencode([
    {
      name              = "nginx"
      image             = "nginx:latest"
      memoryReservation = 128

    }
  ])
  network_mode = "bridge"
}

resource "aws_ecs_service" "nginx" {
  name            = "nginx"
  cluster         = aws_ecs_cluster.ecs.arn
  task_definition = aws_ecs_task_definition.nginx.arn
  desired_count   = 2
}
