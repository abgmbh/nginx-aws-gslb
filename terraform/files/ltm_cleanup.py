#!/usr/bin/env python3.11

import boto3
import requests
import base64
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def send_curl_request(url, method='GET', data=None):
    user = 'bigipuser'
    password = 'YATHzLV1pFOAJsCu'
    CRED_B64 = base64.b64encode(
        (user + ':' + password).encode('utf-8')).decode('utf-8')

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + CRED_B64
    }

    request_data = None if data == None else json.dumps(data)
    response = requests.request(
        method, url, data=request_data, headers=headers, verify=False)

    if response.status_code == 200:
        if method == 'GET':
            return response.json()
        else:
            print(response.text)
            return response.text
    else:
        raise Exception(f'Request failed. Status code: {response.status_code}')


# Create an EC2 client
ec2 = boto3.client('ec2')

# Get instances with a Name tag value of "nginx"
response = ec2.describe_instances(
    Filters=[
        {'Name': 'tag:Name',
         'Values': ['nginx-g1']}
    ]


)

# Extract the private IP addresses and public DNS names of the instances
private_ip_addresses = []

for reservation in response['Reservations']:
    for instance in reservation['Instances']:
        if instance['State']['Name'] != 'terminated':
            private_ip_addresses.append(instance['PrivateIpAddress'])

print(private_ip_addresses)

ltm = '54.79.99.55'

url = f"https://{ltm}/mgmt/tm/sys/config"
vip_url = f'https://{ltm}/mgmt/tm/ltm/virtual'
pool_url = f'https://{ltm}/mgmt/tm/ltm/pool'
node_url = f'https://{ltm}/mgmt/tm/ltm/node'

payload = {
    "command": "save"
}

vip_content = send_curl_request(vip_url, method='GET')
vip_names = [item['name'] for item in vip_content['items']]
print(vip_names)

pool_content = send_curl_request(pool_url, method='GET')
pool_names = [item['name'] for item in pool_content['items']]
print(pool_names)

node_content = send_curl_request(node_url, method='GET')
node_names = [item['name'] for item in node_content['items']]
print(node_names)

# Delete stale vip, pools and nodes

new_octet = '169'
mod_ip = [
    f'{new_octet}.{ip.split(".")[1]}.{ip.split(".")[2]}.{ip.split(".")[3]}' for ip in private_ip_addresses]

for name in set(vip_names) - set(mod_ip):
    if name != 'fast_telemetry_local':
        print(name)
        vip_url = f"https://{ltm}/mgmt/tm/ltm/virtual/{name}"
        send_curl_request(vip_url, method='DELETE')
        print(f"Successfully deleted vip {name}")

for name in set(pool_names) - set(private_ip_addresses):
    if name != 'fast_telemetry':
        print(name)
        pool_url = f"https://{ltm}/mgmt/tm/ltm/pool/{name}"
        send_curl_request(pool_url, method='DELETE')
        print(f"Successfully deleted pool {name}")

for name in set(node_names) - set(private_ip_addresses):
    if name.startswith("10."):
        print(name)
        node_url = f"https://{ltm}/mgmt/tm/ltm/node/{name}"
        send_curl_request(node_url, method='DELETE')
        print(f"Successfully deleted node {name}")

response = send_curl_request(url, method='POST', data=payload)
if "/var/run/f5pcimap: OK\\n" in response:
    print('Saved')
else:
    print('Save failed')
