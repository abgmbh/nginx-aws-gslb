resource "aws_instance" "ubuntu" {
  count                  = 3
  ami                    = var.nginxPlus_ami
  instance_type          = "t2.medium"
  key_name               = aws_key_pair.ssh_access.key_name
  subnet_id              = module.vpc.public_subnets[count.index]
  vpc_security_group_ids = [aws_security_group.nginx.id]

  source_dest_check = false

  tags = {
    Name = "cz_Ubuntu_Instance ${count.index + 1}"
  }
}
