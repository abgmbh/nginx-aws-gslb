provider "aws" {
  region = var.region
}

resource "random_id" "id" {
  byte_length = 4
  prefix      = "cz-"
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "random_string" "this" {
  length  = 12
  special = false
}

locals {
  vpc_cidr = "10.1.0.0/16"
  my_ip    = chomp(data.http.myip.response_body)
}

data "aws_availability_zones" "this" {
  state = "available"
  filter {
    name   = "region-name"
    values = [var.region]
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = random_id.id.dec
  cidr = local.vpc_cidr
  azs = [data.aws_availability_zones.this.names[0],
    data.aws_availability_zones.this.names[1],
  data.aws_availability_zones.this.names[2]]
  public_subnets       = [cidrsubnet(local.vpc_cidr, 8, 1), cidrsubnet(local.vpc_cidr, 8, 2), cidrsubnet(local.vpc_cidr, 8, 3)]
  enable_nat_gateway   = false
  enable_dns_hostnames = true
  tags                 = var.resource_tags
}

resource "aws_key_pair" "ssh_access" {
  public_key = var.ssh_public_key
}

resource "aws_route" "containers_to_host1" {
  route_table_id         = module.vpc.public_route_table_ids[0]
  destination_cidr_block = "192.168.1.0/24"
  network_interface_id   = aws_instance.docker_hosts[0].primary_network_interface_id
}

resource "aws_route" "containers_to_host2" {
  route_table_id         = module.vpc.public_route_table_ids[0]
  destination_cidr_block = "192.168.2.0/24"
  network_interface_id   = aws_instance.docker_hosts[1].primary_network_interface_id
}


