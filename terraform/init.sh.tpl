#!/usr/bin/env bash

PRIVATE_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4) 
FQDN=$(curl http://169.254.169.254/latest/meta-data/public-hostname)

LTM_VIP=$(echo $PRIVATE_IP | sed -r 's/^[0-9]{1,3}/169/')

CRED_B64=$(echo -ne ${user}:${pass} | base64)
LTM=${ltm}
GTM=${gtm}

curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$LTM/mgmt/tm/ltm/node -d "{\"name\":\"$PRIVATE_IP\",\"address\":\"$PRIVATE_IP\"}"

curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$LTM/mgmt/tm/ltm/pool -d "{\"name\":\"$PRIVATE_IP\",\"monitor\":\"/Common/http\",\"members\":\"$PRIVATE_IP:80\"}"

curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$LTM/mgmt/tm/ltm/virtual -d  "{\"name\":\"$LTM_VIP\",\"destination\":\"$LTM_VIP:80\",\"ipProtocol\":\"tcp\",\"profiles\":\"/Common/fastL4\",\"pool\":\"$PRIVATE_IP\"}"

curl -k https://acm.acm-demo.aws.abbagmbh.de/install/nginx-agent | bash -s -- --instance-group ${group}

systemctl start nginx-agent

# create a pool of A type

POOLNAME=$(echo $LTM_VIP | sed -r 's/^/p_/')

curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$GTM/mgmt/tm/gtm/pool/a -d "{\"name\": \"$POOLNAME\"}"

runtime="5 minute"
endtime=$(date -ud "$runtime" +%s)
status=0
while [[ $(date -u +%s) -le $endtime ]]; do

    # associate member to pool
    status=$(curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$GTM/mgmt/tm/gtm/pool/a/~Common~$POOLNAME/members/ -d "{\"name\": \"/Common/DC1_LTM:/Common/$LTM_VIP\",
    \"partition\": \"Common\"}" -w "%%{http_code}" -s -o /dev/null)

    if [ $status -eq 200 ]
    then
    echo "Code returned a status of 200. Exiting loop."
    break
    fi

    sleep 5
done

if [ $status -ne 200 ]
then
    exit 1
fi

while [[ $(date -u +%s) -le $endtime ]]; do

    # create an A WIDEIP and attach to pool
    status=$(curl -k -X POST -H "Content-Type: application/json" -H "Authorization: Basic $CRED_B64" https://$GTM/mgmt/tm/gtm/wideip/a -d "{\"name\": \"$FQDN\", \"pools\": [
        {
            \"name\": \"$POOLNAME\",
            \"partition\": \"Common\",
            \"nameReference\": 
            {
                \"link\": \"https://localhost/mgmt/tm/gtm/pool/a/~Common~$POOLNAME\"
            }
        }
    ]
    }" -w "%%{http_code}" -s -o /dev/null)

    if [ $status -eq 200 ]
    then
    echo "Code returned a status of 200. Exiting loop."
    break
    fi

    sleep 5
done

if [ $status -ne 200 ]
then
    exit 1
fi